---
layout: page
title: About me
permalink: /about
intro: |
    I'm Riccardo. I enjoy new challenges and learning new things. I'm an incurable optimist who loves implementing tools to enable people working on their goals, and finding solutions to hard problems.

---

Currently, I'm working on [Google Distributed Cloud Hosted] (GDCH), Google's digital sovereignty vision for Europe, helping our partners to support public-sector customers and commercial entities to address data residency and strict security and privacy requirements. 

<figure>
    <img src="https://img.rpadovani.com/posts/about_me.png" alt="About me" />
    <figcaption>
      <p><span>Illustration by <a href="https://plus.undraw.co/" target="_blank">unDraw+</a>.</span></p>
    </figcaption>
</figure>

I earned a Master's Degree in Computer Science, graduating from Technische Universität München, with a focus on Security Engineering and Language Theory.

# My daily job

As a Cloud Infrastructure Engineer, I am focusing mainly on helping our partners running GDCH, providing custom support, training, and technical expertise.

Previously, I worked as Platform Engineer for [Dock Financial][dock], as Solutions Architect at [Nextbit], and as JavaScript Developer at [Fleetster], and some other roles. You can find a more precise timeline on my [LinkedIn].

# Opensource contributions
 
Big part of my passion, and my knowledge, for IT related topics, is due opensource. I started contributing to Ubuntu when I was young, becoming quickly a [Ubuntu member]. I wrote different apps for Ubuntu for Phones, including [100 Balls], [Discerning Duck], and [Falldown]. 

Nowadays, the huge majority of my contributions go towards GitLab, being them code patches, issues, advocacy, or simple support on StackOverflow or the GitLab forum. Following all the contributions, I have been recognized as a [GitLab Hero]. 

# Bug bounties

<small>As RMS said, hacking means exploring the limits of what is possible, in a spirit of playful cleverness.</small>

I like breaking things to understand how they work, and _hacking_ exercises, of course, a considerable appeal on me. Among the other things, sometimes I have fun reporting security issues on different websites. You can find a report of my 2019's activity on in [this blog post][hackerone-2019]. An always up-to-date source of my activity can be found on [HackerOne].

# The rest of my life

While my online-persona is quite focused on C.S. related stuffs, I have an entire life out of the internet, with a funny Italian accent, enjoying life with my S.O. in Munich. If you want to know me better, just drop me an email, and if you are ever in Bavaria, let's have a beer!

# Contact me

Find me on [LinkedIn] or just
say `Hello` at [hello@rpadovani.com](mailto:hello@rpadovani.com).

[Google Distributed Cloud Hosted]: https://cloud.google.com/distributed-cloud
[gitlab]: https://gitlab.com/rpadovani
[github]: https://github.com/rpadovani
[launchpad]: https://launchpad.net/~rpadovani/
[100 balls]: https://github.com/rpadovani/100balls
[discerning duck]: https://github.com/rpadovani/discerning-duck
[coreapps]: https://launchpad.net/ubuntu-phone-coreapps
[reminders]: https://launchpad.net/reminders-app
[calculator]: https://launchpad.net/ubuntu-calculator-app
[ubuntuit]: http://www.ubuntu-it.org
[archon]: http://www.archon.ai/
[archonpost]: https://rpadovani.com/my-first-job
[linkedin]: https://www.linkedin.com/in/riccardopadovani
[browser]: https://launchpad.net/webbrowser-app
[falldown]: https://uappexplorer.com/app/falldown.rpadovani
[cv]: https://cv.rpadovani.com
[fleetster]: http://www.fleetster.net/
[nextbit]: https://nextbit.it
[dock]: https://dock.financial/en/
[ubuntu member]: https://wiki.ubuntu.com/RiccardoPadovani
[gitlab hero]: https://about.gitlab.com/community/heroes/members/
[hackerone-2019]: https://rpadovani.com/2019-hackerone
[hackerone]: https://hackerone.com/rpadovani
