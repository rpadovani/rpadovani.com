---
layout: page
title: Privacy
permalink: /privacy
intro: "I respect your privacy :-)"
---

There are no analytics on this website. It uses however hyvor.com as a comment system.

## Cookies

This website installs only technical cookies, and only if you comment.

## Comments

This website uses [Hyvor Talk][hyvor] service as the commenting platform. The comments and other data exchanged are stored securely within the Hyvor Talk system. Your personal data will be processed and transmitted in accordance with the General Data Protection Regulation (GDPR). For more information, please refer to [their privacy policy][their-policy].


[hyvor]: https://talk.hyvor.com/
[their-policy]: https://talk.hyvor.com/docs/privacy
