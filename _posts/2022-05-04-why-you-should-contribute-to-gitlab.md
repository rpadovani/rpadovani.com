---
layout: post
title:  "Why you should contribute to GitLab"
date:   2022-05-04
description: "Contributing to an opensource project is an amazing experience: let me share with you mine, and give some tips"
categories:
- gitlab
permalink: contribute-to-gitlab
cover: https://img.rpadovani.com/posts/gitlab/gitlab-logo-gray-rgb.png
---

Contributing to any open-source project is a great way to spend a few hours each month. I started more than 10 years ago, and it has ultimately shaped my career in ways I couldn't have imagined!

<figure>
    <img src="https://img.rpadovani.com/posts/gitlab/gitlab-logo-gray-rgb.svg" alt="GitLab logo as cover" />
    <figcaption>
      <p><span>The new GitLab logo, <a href="https://about.gitlab.com/blog/2022/04/27/devops-is-at-the-center-of-gitlab/" target="_blank">just announced</a> on the 27th April 2022.</span></p>
    </figcaption>
</figure>

Nowadays, my contributions focus mostly on [GitLab], so you will see many references to it in this blog post, but the content is quite generalizable; I would like to share my experience to highlight why you should consider contributing to an open-source project.

<small>Writing blog posts, tweeting, and helping foster the community are nice ways to contribute to a project ;-)</small>

And contributing doesn't mean only coding: there are countless ways to help an open-source project: **translating** it to different languages, **reporting** issues, **designing** new features, **writing** the documentation, offering **support** on forums and [StackOverflow], and so on.

Before deep diving in this wall of text, be aware there are mainly three parts in this blog post, after this introduction: a _context_ section, where I describe my personal experience with open source, and what does it mean to me. Then, a nice _list of reasons_ to contribute to any project. In closing, there are some _tips_ on how to start contributing, both in general, and something specific to GitLab.

# Context

Ten years ago, I was fresh out of high school, without (almost) any knowledge about IT: however, I found out that I had a massive passion for it, so I enrolled in a Computer Engineering university (boring!), and I started contributing to [Ubuntu] (cool!). I began with the Italian Local Community, and I soon moved to [Ubuntu Touch].

<small>I often considered rewriting that old article: however I have a strange attachment to it as it is, with all that English mistakes. One of the first blog posts I have written, and it was really well received!</small>
We all know how it ended, but it still has been a fantastic ride, with a lot of great moments: just take a look at the [archive] of this blog, and you can see the passion and the enthusiasm I had. I was so enthusiast, that I wrote a [similar blog post] to this one! I think it highlights really well some considerable differences 10 years make.

Back then, I wasn't working, just studying, so I had a lot of spare time. My English was _way_ worse.  I was at the beginning of my journey in the computer world, and Ubuntu has ultimately shaped a big part of it. My knowledge was very limited, and I never worked before. Contributing to Ubuntu gave me a glimpse of real world, I met outstanding engineers who taught me a lot, and boosted my CV, helping me to land [my first job].

<small> Advocacy, as in this blog post, is a great way to contribute! You spread awareness, and this helps to find new contributors, and maybe inspiring some young student to try out!</small>
Since then, I completed a master's degree in C.S., worked in different companies in three different countries, and became a professional. Nowadays, my contributions to open source are more sporadic (adulthood, yay), but given how much it meant to me, I am still a big fan, and I try to contribute when I can, and how I can. 

# Why contributing

## Friends

During my years contributing to open-source software, I've met countless incredible people, with some of whom I've become friend. In the old blog post I mentioned [David]: in the last 9 years we stayed in touch, met in different occasions in different cities: last time was as recent as last summer. Back at the time, he was a Manager in the Ubuntu Community Team at Canonical, and then, he became Director of Community Relations at GitLab. Small world!

<figure>
    <img src="https://img.rpadovani.com/posts/maltasprint.jpg" alt="The Ubuntu Touch Community Team in Malta, in 2014" />
    <figcaption>
      <p><span>The Ubuntu Touch Community Team in <a href="https://rpadovani.com/canonical-sprint-in-malta" target="_blank">Malta</a>, in 2014. It has been an incredible week, sponsored by Canonical!</span></p>
    </figcaption>
</figure>

One interesting thing is people contribute to open-source projects from their homes, all around the world: when I travel, I usually know somebody living in my destination city, so I've always at least one night booked for a beer with somebody I've met only online; it's a pleasure to speak with people from different backgrounds, and having a glimpse in their life, all united by one common passion.

## Fun

Having fun is important! You cannot spend your leisure time getting bored or annoyed: contributing to open source is fun 'cause you pick the problems you would like to work on, and you don't need all that _bureaucracy_ and _meetings_ that is often needed in your daily job. You can be challenged, and feeling useful, and improving a product, without any manager on your shoulder, and with your pace. 

## Being up-to-date on how things evolve

<small>For example, the <a href="https://about.gitlab.com/handbook/" target="_blank">GitLab Handbook</a> is a precious collection of resources, ideas, and methodologies on how to run a 1000 people company in a transparent, full remote, way. It's a great reading, with a lot of wisdom.</small>

Contributing to a project typically gives you an idea on how teams behind it work, which technologies they use, and which methodologies. Many open-source projects use bleeding-edge technologies, or draw a path. Being in contact with new ideas is a great way to know where the industry is headed, and what are the latest news: it is especially true if you hang out in the channels where the community meets, being them Discord, forums, or IRC (well, IRC is not really bleeding-edge, but it is _fun_).

## Learning

When contributing in an area that doesn't match your expertise, you always learn something new: reviews are usually precise and on point, and projects of a remarkable size commonly have  a coaching team that help you to start contributing, and guide you on how to land your first patches.

In GitLab, if you need a help in merging your code, there are the [Merge Request Coaches]! And for any type of help, you can always join [Gitter], or ask on the [forum], or write to the [dedicated email address].

Feel also free to [ping me] directly if you want some general guidance!

## Giving back

I work as a Platform Engineer. My job is built on an incredible amount of open-source libraries, amazing FOSS services, and I basically have just to glue together different pieces. When I find some rough edge that could be improved, I try to do so. 

Nowadays, I find crucial having well-maintained documentation, so after I have achieved something complex, I usually go back and try to improve the documentation, where lacking. It is my tiny way to say thanks, and giving back to a world that really has shaped my career.

This is also what mostly of my blogs posts are about: after having completed something on which I spent fatigue on, I find it nice being able to share such information. Every so often, I find myself years later following my guide, and I really also appreciate when other people find the content useful.

## Swag 

Who doesn't like [swag]? :-) Numerous projects have delightful swags, starting from stickers, that they like to share with the whole community. Of course, it shouldn't be your main driver, 'cause you will soon notice that it is ultimately not worth the amount of time you spend contributing, but it is charming to have GitLab socks!

<figure>
    <img src="https://img.rpadovani.com/posts/2019-hackerone/keyboard.jpg" alt="A GitLab branded mechanical keyboard" />
    <figcaption>
      <p><span>A GitLab branded mechanical keyboard, courtesy of the GitLab's security team! This very article has been typed with it!</span></p>
    </figcaption>
</figure>


# Tips

I hope I  inspired you to contribute to some open-source project (maybe GitLab!). Now, let's talk about some small tricks on how to begin easily.

## Find something you are passionate about

You must find a project you are passionate about, and that you use frequently. Looking forward to a release, knowing that your contributions will be included, it is a **wonderful satisfaction**, and can really push you to do more.

Moreover, if you already know the project you want to contribute to, you probably know already the biggest pain points, and where the project needs some contributions.

## Start small and easy
You don't need to do gigantic contributions to begin. Find something tiny, so you can get familiar with the project workflows, and how contributions are received.

<small>Launchpad and bazaar instead of GitLab and git — down the memory lane!</small>
My journey with Ubuntu started [correcting a typo] in a README, and here I am, years later, having contributed to dozens of projects, and having a career in the C.S. field. Back then, I really had no idea of what my future would have held.

For GitLab, you can take  a look at the issues marked as “[good for new contributors]”. They are designed to be addressed quickly, and onboard new people in the community. In this way, you don't have to focus on the difficulties of the task at hand, but you can easily explore how the community works.

## Writing issues is a good start

Writing high-quality issues is a great way to start contributing: maintainers of a project are not always aware of how the software is used, and cannot be aware of all the issues. If you know that something could be improved, write it down: spend some time explicating what happens, what you expect, how to reproduce the problem, and maybe suggest some solutions as well! Perhaps, the first issue you write down could be the <u>very first issue you resolve</u>.

## Not much time required!

Contributing to a project doesn't require necessarily a lot of time. When I was younger, I definitely dedicated way more time to open-source projects, implementing gigantic features. Nowadays, I don't do that anymore (life is much more than computers), but I like to think that my contributions are still useful.  Still, I don't spend more than a couple of hours a month, based on my schedule, and how much is raining (yep, in winter I definitely contribute more than in summer).

## GitLab is super easy

Do you use GitLab? Then you should undoubtedly try to contribute to it. It is easy, it is fun, and there are many ways. Take a look at [this guide], hang out on [Gitter], and see you around. ;-)

Next week (9th-13th May 2022) there is also a [GitLab Hackathon]! It is a real fun and easy way to start contributing: many people are available to help you, there are video sessions talking about contributing, and just doing a small contribution you will receive a pretty [prize].

<small>And if I was able to do it with my few contributions, you can as well!</small>
And in time, if you are consistent in your contributions, you can become a [GitLab Hero]! How cool is that?

I really hope this wall of text made you consider contributing to an open-source project. If you have any question, or feedback, or if you would like some help, please leave a comment below, or write me an email at [hello@rpadovani.com][email].

  
Ciao,  
R.

[email]: mailto:hello@rpadovani.com

[GitLab]: https://about.gitlab.com/
[StackOverflow]: https://stackoverflow.com/collectives/gitlab
[Ubuntu]: https://ubuntu.com/
[Ubuntu Touch]: https://en.wikipedia.org/wiki/Ubuntu_Touch
[archive]: https://rpadovani.com/blog
[similar blog post]: https://rpadovani.com/why-you-should-contribute-to-ubuntu-touch
[my first job]: https://rpadovani.com/my-first-job
[David]: https://www.linkedin.com/in/davidplanella/
[Merge Request Coaches]: https://about.gitlab.com/job-families/expert/merge-request-coach/
[Gitter]: https://gitter.im/gitlabhq/contributors
[forum]: https://forum.gitlab.com
[dedicated email address]: mailto:contributors@gitlab.com
[ping me]: mailto:hello@rpadovani.com
[swag]: https://shop.gitlab.com/shop/sale/
[correcting a typo]: https://code.launchpad.net/~rpadovani/phablet-tools/fix-for-1139999/+merge/153419
[good for new contributors]: https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=good+for+new+contributors
[this guide]: https://about.gitlab.com/community/contribute/
[GitLab Hackathon]: https://about.gitlab.com/community/hackathon/
[prize]: https://about.gitlab.com/community/hackathon/#prize
[GitLab Hero]: https://about.gitlab.com/community/heroes/
